secret_earth_spirituality_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_earth_spirituality
	}
}

secret_ba_sing_se_cultural_heritage_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_ba_sing_se_cultural_heritage
	}
}

secret_omashuan_ethics_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_omashuan_ethics
	}
}

secret_path_of_jin_wei_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_path_of_jin_wei
	}
}

secret_path_of_wei_jin_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_path_of_wei_jin
	}
}


secret_the_great_serpent_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_the_great_serpent
	}
}

secret_the_divided_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_the_divided
	}
}

secret_fortune_telling_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_fortune_telling
	}
}

secret_unificationism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_unificationism
	}
}

secret_way_of_chin_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_way_of_chin
	}
}

secret_followers_of_kyoshi_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_followers_of_kyoshi
	}
}

secret_earth_monarchism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_earth_monarchism
	}
}

secret_children_of_si_wong_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_children_of_si_wong
	}
}

secret_fire_spirituality_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_fire_spirituality
	}
}

secret_fire_nation_imperialism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_fire_nation_imperialism
	}
}

secret_bhanti_spirituality_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_bhanti_spirituality
	}
}

secret_the_eternal_flame_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_the_eternal_flame
	}
}

secret_nomadic_pacifism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_nomadic_pacifism
	}
}

secret_nomadic_nihilism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_nomadic_nihilism
	}
}

secret_air_acolytism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_air_acolytism
	}
}

secret_neo-nomadism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_neo-nomadism
	}
}

secret_southern_spirituality_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_southern_spirituality
	}
}

secret_norse_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_norse_pagan
	}
}

secret_tengri_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_tengri_pagan
	}
}

secret_baltic_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_baltic_pagan
	}
}

secret_finnish_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_finnish_pagan
	}
}

secret_aztec_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_aztec
	}
}

secret_aztec_reformed_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_aztec_reformed
	}
}

secret_slavic_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_slavic_pagan
	}
}

secret_west_african_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_west_african_pagan
	}
}

secret_zun_pagan_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_zun_pagan
	}
}

secret_the_great_swamp_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_the_great_swamp
	}
}

secret_witchcraft_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_witchcraft
	}
}

secret_tui_and_la_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_tui_and_la
	}
}

secret_vaatuism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_vaatuism
	}
}

secret_modernism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_modernism
	}
}

secret_anarchism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_anarchism
	}
}

secret_piracy_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_piracy
	}
}

secret_barbarism_community = {
	icon = 18
	is_visible = {
		society_member_of = secret_religious_society_barbarism
	}
}